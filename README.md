# Jobtech Crossplane configuration

This repository creates a Crossplane composite configuration to simplify resources
to deploy.

You can find example resources it provides API for in examples/resources.

## Before you start

Ensure you have:

* kubectl installed
* [Crossplane CLI](https://crossplane.io/docs/v1.7/getting-started/install-configure.html#install-crossplane-cli) installed
* logged in to destination container registry.

At JobTech you login to our container registry with your LDAP credentials.
With podman do: `podman login docker-images.jobtechdev.se`.
With docker do: `docker login docker-images.jobtechdev.se`.

## Build configuration

```shell
cd configuration
rm *.xpkg
kubectl crossplane build configuration
kubectl crossplane push configuration docker-images.jobtechdev.se/crossplane-config/jobtech-configuration:vX.Y.Z
```

Ensure to update `vX.Y.Z` to the current version number.

Installation of this configuration package is done by
[crossplane-infra repo](https://gitlab.com/arbetsformedlingen/devops/crossplane/crossplane-infra).
